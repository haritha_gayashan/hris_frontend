import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SysuserComponent } from './admin/sysuser/sysuser.component';
import { TestComponentComponent } from './dashboard/test-component/test-component.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { MidcontentComponent } from './midcontent/midcontent.component';
import { RolePermissionComponent } from './settings/role-permission/role-permission.component';

const routes: Routes = [
  {path: '' , component:LoginComponent},
  { path: 'test2', component: TestComponentComponent },
      { path: 'dashboard', component: MidcontentComponent, canActivate:[AuthGuard]},
      { path: 'user', component: SysuserComponent, canActivate:[AuthGuard] },
      {path:'user-rols-permissions', component:RolePermissionComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
