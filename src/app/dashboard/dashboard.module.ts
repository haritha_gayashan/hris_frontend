import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router'
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { KeyPerformaceIndicatorComponent } from './key-performace-indicator/key-performace-indicator.component';
import { KpiMeterComponent } from './kpi-meter/kpi-meter.component';
import { TestComponentComponent } from './test-component/test-component.component';
import { EmployeeMetricsComponent } from './employee-metrics/employee-metrics.component';
import { CalendarComponent } from './calendar/calendar.component';

@NgModule({
  declarations: [NavbarComponent, SidebarComponent, KeyPerformaceIndicatorComponent, KpiMeterComponent, TestComponentComponent, EmployeeMetricsComponent, CalendarComponent],
  imports: [CommonModule,
    RouterModule.forRoot([
    {path:'test',component:CalendarComponent},
  ])
  ],
  exports: [NavbarComponent, SidebarComponent, KpiMeterComponent, TestComponentComponent, KeyPerformaceIndicatorComponent, EmployeeMetricsComponent, CalendarComponent],
})
export class DashboardModule {}
