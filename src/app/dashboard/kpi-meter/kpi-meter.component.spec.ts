import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiMeterComponent } from './kpi-meter.component';

describe('KpiMeterComponent', () => {
  let component: KpiMeterComponent;
  let fixture: ComponentFixture<KpiMeterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KpiMeterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiMeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
