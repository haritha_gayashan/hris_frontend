import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router'
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { AppRoutingModule } from './app-routing.module';
import { CalendarComponent } from './dashboard/calendar/calendar.component';
import { TestComponentComponent } from './dashboard/test-component/test-component.component';
import { MidcontentComponent } from './midcontent/midcontent.component';

import { SysuserComponent } from './admin/sysuser/sysuser.component';
import { AdminModule } from './admin/admin.module';
import { SettingsModule } from './settings/settings.module';
import { RolePermissionComponent } from './settings/role-permission/role-permission.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AppComponent, MidcontentComponent, LoginComponent],
  imports: [BrowserModule, DashboardModule, AppRoutingModule,AdminModule,SettingsModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
