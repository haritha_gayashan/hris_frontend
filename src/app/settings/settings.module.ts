import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RolePermissionComponent } from './role-permission/role-permission.component';



@NgModule({
  declarations: [RolePermissionComponent],
  imports: [
    CommonModule
  ]
})
export class SettingsModule { }
