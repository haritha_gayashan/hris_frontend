import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SysuserComponent } from './sysuser/sysuser.component';



@NgModule({
  declarations: [SysuserComponent],
  imports: [
    CommonModule
  ],
  exports : [SysuserComponent]
})
export class AdminModule {  }
