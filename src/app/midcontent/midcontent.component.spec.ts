import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MidcontentComponent } from './midcontent.component';

describe('MidcontentComponent', () => {
  let component: MidcontentComponent;
  let fixture: ComponentFixture<MidcontentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MidcontentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MidcontentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
